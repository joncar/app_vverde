var url = 'http://166.62.46.114/PVEM/bingo.asmx/login';
//var url = 'http://evolutiontrack-sur.ddns.net/wsPadron/gps.asmx?op=login';
angular.module('starter.controllers', [])

.controller('Login', function($scope, $http, $state,$ionicLoading,Api,User,$ionicPopup) {
    console.log(User.getData())
    if(User.getData()!=null){
        document.location.href="main.html";
    }
    else{
        $scope.loading = function(attr){            
            attr=='hide'?$ionicLoading.hide():$ionicLoading.show({template: 'Cargando...'});
        };
        $scope.showAlert = function(title,template) {
            var alertPopup = $ionicPopup.alert({
              title: title,
              template: template
            });
         };
        $scope.connect = function(inputs){
            $scope.data = 'username='+inputs.username+'&password='+inputs.pass;
            $scope.user = {username:inputs.username};
            Api.query('',$scope,$http,function(data){
                data = JSON.parse(data.string.__text);
                data = data[0];
                if(data.IdZona!=undefined){
                    localStorage.idzona = data.IdZona;
                    User.setData($scope.user)
                    document.location.href="main.html";
                }
                else
                $scope.showAlert('Login incorrecto','Usuario o contraseña incorrecto.')
            });/*
            document.location.href="main.html";*/
        }
    }
});