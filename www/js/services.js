angular.module('starter.services', [])

.factory('Api', function() {
  var email = '';
  var password = '';
  var data = "";
  return {
    list:function(controller,data,$scope,$http,successFunction){
        if(typeof(data)=='object'){
            s = '';
            for(i in data){
                s+= 'search_field[]='+i+'&search_text[]='+data[i]+'&';
            }
            data = s;
        }
        $scope.loading('show');
        $http({
            url:url+controller+'/json_list',
            method: "POST",
            data:data,
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            }           
          }).success(function(data){
             if(successFunction!==undefined){
                 successFunction(data);
                 $scope.loading('hide');
             }
          });
    },
    insert:function(controller,$scope,$http,successFunction){
       if(!$scope.data){//Si ls datos son de un formulario
            d = document.getElementById('formreg');
            data = new FormData(d);  
        }//Si se envia el array
        else{
            data = new FormData();
            for(i in $scope.data){
              data.append(i,$scope.data[i]);
            }            
        }
        $scope.loading('show');     
       $http({
            url:url+controller+'/insert_validation',
            method: "POST",
            data:data,
            transformRequest: angular.identity,
            headers: {
              'Content-Type':undefined
            }           
          }).success(function(data){
              data = data.replace('<textarea>','');
              data = data.replace('</textarea>','');
              data = JSON.parse(data);
              if(data.success){
                  if(!$scope.data){//Si ls datos son de un formulario
                        d = document.getElementById('formreg');
                        data = new FormData(d);  
                  }//Si se envia el array
                  else{
                      data = new FormData();
                      for(i in $scope.data){
                        data.append(i,$scope.data[i]);
                      }            
                  }       
                  $http({
                        url:url+controller+'/insert',
                        method: "POST",
                        data:data,
                        transformRequest: angular.identity,
                        headers: {
                          'Content-Type':undefined
                        }           
                      }).success(function(data){
                          data = data.replace('<textarea>','');
                          data = data.replace('</textarea>','');
                          data = JSON.parse(data);                          
                          if(data.success){
                               if(successFunction!==undefined){
                                   successFunction(data);
                                   $scope.loading('hide');
                               }
                          }
                          else{
                             $scope.loading('hide');
                             $scope.showAlert('Ocurrio un error al añadir','Ocurrio un error al añadir');
                          }
                        })
                        .error(function(){
                            alert('Ha ocurrido un error interno, contacte con un administrador');
                        });
              }
              else{
                  $scope.loading('hide');
                  $scope.showAlert('Ocurrio un error al añadir',data.error_message);
              }
          })
          .error(function(data){
              alert('Ha ocurrido un error interno, contacte con un administrador');
          });
        },
        
        update:function(controller,id,$scope,$http,successFunction){
        if(!$scope.data){//Si ls datos son de un formulario
          d = document.getElementById('formreg');
          data = new FormData(d);
        }//Si se envia el array
        else{
          data = new FormData();
          for(i in $scope.data){
            data.append(i,$scope.data[i]);
          }
        }
        $scope.loading('show');            
        $http({
             url:url+controller+'/update_validation/'+id,
             method: "POST",
             data:data,
             transformRequest: angular.identity,
             headers: {
               'Content-Type':undefined
             }           
           }).success(function(data){
               data = data.replace('<textarea>','');
               data = data.replace('</textarea>','');
               data = JSON.parse(data);
               if(data.success){
                    if(!$scope.data){//Si ls datos son de un formulario
                      d = document.getElementById('formreg');
                      data = new FormData(d);
                    }//Si se envia el array
                    else{
                      data = new FormData();
                      for(i in $scope.data){
                        data.append(i,$scope.data[i]);
                      }
                    }       
                   $http({
                         url:url+controller+'/update/'+id,
                         method: "POST",
                         data:data,
                         transformRequest: angular.identity,
                         headers: {
                           'Content-Type':undefined
                         }           
                       }).success(function(data){
                           data = data.replace('<textarea>','');
                           data = data.replace('</textarea>','');
                           data = JSON.parse(data);                          
                           if(data.success){
                                if(successFunction!==undefined){
                                   successFunction(data);
                                   $scope.loading('hide');
                                }
                           }
                           else{
                              $scope.loading('hide');
                              $scope.showAlert('Ocurrio un error al añadir','Ocurrio un error al añadir');
                           }
                    });
               }
               else{
                   $scope.loading('hide');
                   $scope.showAlert('Ocurrio un error al añadir',data.error_message);
               }
          });
        },
        
        deleterow:function(controller,id,$scope,$http,successFunction){        
        $scope.loading('show');
        $http({
             url:url+controller+'/delete/'+id,
             method: "GET",
             data:'',
             transformRequest: angular.identity,
             headers: {
               'Content-Type':undefined
             }           
           }).success(function(data){
               
               if(data.success){
                    if(successFunction!==undefined){
                        successFunction(data);
                        $scope.loading('hide');
                     }
               }
               else{
                   $scope.loading('hide');
                   $scope.showAlert('Ocurrio un error al eliminar',data.error_message);
               }
          });
        },

        initInterface:function($scope){

          return $scope;
        },

        query:function(controller,$scope,$http,successFunction){
            if(!$scope.data){//Si ls datos son de un formulario
              d = document.getElementById('formreg');
              data = new FormData(d);
            }
            data = $scope.data;
            $scope.loading('show');            
            $http({
                 url:url+controller,
                 method: "POST",
                 data:data,
                 transformRequest: angular.identity,
                 headers: {
                   'Content-Type':'application/x-www-form-urlencoded; charset=utf-8'
                 }           
               }).success(function(data){                   
                  var x2js = new X2JS();
                  var data = x2js.xml_str2json(data);
                  if(successFunction!==undefined){
                    successFunction(data);
                    $scope.loading('hide');
                 }
                }).error(function(data){
                  $scope.loading('hide');
                  $scope.showAlert('Ocurrio un error al enviar los datos',data);
              });
        }
    }  
})

.factory('User', function() {  
  return {
        setData:function(data){
            localStorage.user = JSON.stringify(data);
        },
        
        getData:function($scope){
            if(localStorage.user==undefined){
                return null;
            }
            else{
                return JSON.parse(localStorage.user);
            }
        },
        
        cleanData:function(){
            localStorage.removeItem('user');
        }
    }  
})


.factory('bd', function() {  
  return {
        addBingo:function(data){
            if(localStorage.bingos==undefined)localStorage.bingos = '[]';
            bingos = JSON.parse(localStorage.bingos);
            bingos.push(data);
            localStorage.bingos = JSON.stringify(bingos);
        },        
        getBingo:function(){
           return localStorage.bingos!=undefined?JSON.parse(localStorage.bingos):[];
        },
        selectBingo:function(zona,seccion,casilla,indice){
           bingos = localStorage.bingos!=undefined?JSON.parse(localStorage.bingos):[];
           if(bingos.length>0){
               for(i in bingos){
                   if(bingos[i].zona==zona,bingos[i].seccion==seccion && bingos[i].casilla==casilla && bingos[i].indice == indice){
                       return bingos[i];
                   }
               }
           }
           else return null;
        },
        destroy:function(){
            localStorage.removeItem('bingos');
        }
    }  
});
