var url = 'http://166.62.46.114/PVEM/bingo.asmx/';
angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope,$state,$http,$ionicLoading,Api,$ionicPopup,bd) {    
    $scope.loading = function(attr){            
        attr=='hide'?$ionicLoading.hide():$ionicLoading.show({template: 'Cargando...'});
    };
    $scope.showAlert = function(title,template) {
        var alertPopup = $ionicPopup.alert({
          title: title,
          template: template
        });
     };
     
    $scope.idzona = localStorage.idzona;
    $scope.seccion = '';
    $scope.data = 'zona='+$scope.idzona;
    Api.query('getSecciones',$scope,$http,function(data){
        data = JSON.parse(data.string.__text);
        d = [];
        for(i in data){            
            d.push(data[i].Seccion);
        }
        $scope.secciones = d;
    });
    
    $scope.consultarCasilla = function(val){
        $scope.seccion = val;
        $scope.data = 'zona='+$scope.idzona+'&secc='+val;
        Api.query('getCasillas',$scope,$http,function(data){
            data = JSON.parse(data.string.__text);
            d = [];
            for(i in data){            
                d.push(data[i].casilla);
            }
            $scope.casillas = d;
        });
    };
    
    $scope.setCasilla = function(val){
        $scope.casilla = val;
    }
    
    $scope.setIndice = function(val){
        $scope.indice = val;
    }
    
    $scope.play = function(){
        if(bd.selectBingo($scope.idzona,$scope.seccion,$scope.casilla,$scope.indice)==null){
            $scope.data = 'zona='+$scope.idzona+'&seccion='+$scope.seccion+'&casilla='+$scope.casilla+'&indice='+$scope.indice;
            bd.addBingo({zona:$scope.idzona,seccion:$scope.seccion,casilla:$scope.casilla,indice:$scope.indice});
            Api.query('bingo',$scope,$http,function(data){
                data = data.string;
                if(data.__text!=undefined){
                    $scope.showAlert('Satisfactorio','Se han enviado los datos satisfactoriamente');
                }
                else{
                    $scope.showAlert('Error','Ha ocurrido un error en el servidor');
                }
            });
        }
        else{
            $scope.showAlert('Error','El dato que intenta enviar ya fue elegido');
        }
    }
})

.controller('ChatsCtrl', function($scope,bd) {
  $scope.chats = bd.getBingo();
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope,bd,User) {
  $scope.unlog = function(){
      bd.destroy();
      User.cleanData();
      document.location.href="index.html";
  }
  
});
